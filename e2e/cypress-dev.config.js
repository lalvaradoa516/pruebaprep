const { defineConfig } = require('cypress')

module.exports = defineConfig({
  fileServerFolder: '.',
  fixturesFolder: './src/fixtures',
  modifyObstructiveCode: false,
  video: true,
  videosFolder: './videos',
  screenshotsFolder:
    './screenshots',
  screenshotOnRunFailure: true,
  chromeWebSecurity: false,
  retries: {
    runMode: 0,
    openMode: 0,
  },
  e2e: {
    setupNodeEvents(on, config) {
      const cucumber = require('cypress-cucumber-preprocessor').default;
      const browserify = require('@cypress/browserify-preprocessor');
      const options = {
        ...browserify.defaultOptions,
        typescript: require.resolve('typescript'),
      };
      on('file:preprocessor', cucumber(options));
      return require('./src/plugins/index.ts')(on, config);
    },
    baseUrl: 'https://prep2023-tamps.ine.mx/senadurias/nacional/circunscripcion2/tamaulipas/votos-ppyci/grafica',
    specPattern: '**/*.{feature,features}',
    supportFile: '**/**/src/support/e2e.ts',
    watchForFileChanges:true
  },
})
