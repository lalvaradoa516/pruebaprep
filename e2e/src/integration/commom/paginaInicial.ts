import { Given,When } from "cypress-cucumber-preprocessor/steps";

const btnConsultaResultadosPreliminares = 'button[class*=btn-inicio]'

Given('Abro la pagina inicial', () => {
  cy.visit(Cypress.config().baseUrl);
})

When('Hago click en el boton consulta resultados preliminares', () => {
  cy.get(btnConsultaResultadosPreliminares)
    .click();
})
