import { When, Then } from "cypress-cucumber-preprocessor/steps";

const selectorImagenPAN='img[src*="PAN"]';
const selectorParrafoNumeroVotos = 'p[class*="tx-ganador"]';

When('Verifico que carga la pantalla de votos por candidatura', () => {
  cy.url().should('contain','votos-candidatura')
})

When('Obtengo los datos de la candidatura PAN PRI PRD', () => {
  cy.get(selectorImagenPAN)
    .parent()
    .parent()
    .parent()
    .parent()
    .find(selectorParrafoNumeroVotos)
    .then((element)=>{
      cy.addValueBetweenSteps('votosCandidaturaPanPriPrd',element[0].textContent)
    })
})

Then('Valido que la suma de los votos por partido sea igual a la de la candidatura PAN PRI PRD', () => {
  cy.returnAllValuesBetweenSteps().then(values=>{
      const votosPAN=parseInt(values.panVotos.trim().replace(',',''));
      const votosPRI=parseInt(values.priVotos.trim().replace(',',''));
      const votosPRD=parseInt(values.prdVotos.trim().replace(',',''));
      const votosSumaPartidos=votosPAN+votosPRI+votosPRD;
      const votosCandidatura=parseInt(values.votosCandidaturaPanPriPrd.trim().replace(',',''));
      cy.wrap({votosTotales:votosSumaPartidos})
        .its('votosTotales')
        .should('eq', votosCandidatura);
  })
})
  

  

