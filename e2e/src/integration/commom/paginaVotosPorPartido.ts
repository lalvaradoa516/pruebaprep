import { When } from "cypress-cucumber-preprocessor/steps";

const selectorImagenPAN = 'img[src*="PAN"]';
const selectorImagenPRI = 'img[src*="PRI"]';
const selectorImagenPRD = 'img[src*="PRD"]';
const selectorParrafoNumeroVotos = 'p[class*="tx-ganador"]';
const textoBotonVotosCandidatura= 'Votos por Candidatura';

When('Verifico que muestre la pagina de votos por partido politico', (title) => {
    cy.url()
      .should('contain','votos-ppyci');
  })

When('Obtengo los datos del partido PAN', () => {
  cy.get(selectorImagenPAN,{timeout:10000})
    .parent()
    .parent()
    .parent()
    .parent()
    .find(selectorParrafoNumeroVotos)
    .then((element)=>{
      cy.addValueBetweenSteps('panVotos',element[0].textContent)
    })
})

When('Obtengo los datos del partido PRI', () => {
  cy.get(selectorImagenPRI,{timeout:10000})
    .parent()
    .parent()
    .parent()
    .parent()
    .find(selectorParrafoNumeroVotos).then((element)=>{
      cy.addValueBetweenSteps('priVotos',element[0].textContent)
    })
})

When('Obtengo los datos del partido PRD', () => {
  cy.get(selectorImagenPRD,{timeout:10000})
    .parent()
    .parent()
    .parent()
    .parent()
    .find(selectorParrafoNumeroVotos)
    .then((element)=>{
      cy.addValueBetweenSteps('prdVotos',element[0].textContent)
    })
})

When('Hago click en el boton votos por candidatura', () => {
  cy.contains(textoBotonVotosCandidatura)
    .click();
})

  
