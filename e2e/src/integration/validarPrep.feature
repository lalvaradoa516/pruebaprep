Feature: Validar informacion de pagina PREP

  Valido que la suma de los votos por partido sea igual a la de la candidatura
  
  Scenario: Opening a search engine page
    Given Abro la pagina inicial
    When Hago click en el boton consulta resultados preliminares
    And Verifico que muestre la pagina de votos por partido politico
    And Obtengo los datos del partido PAN
    And Obtengo los datos del partido PRI
    And Obtengo los datos del partido PRD
    And Hago click en el boton votos por candidatura
    And Verifico que carga la pantalla de votos por candidatura
    And Obtengo los datos de la candidatura PAN PRI PRD
    Then Valido que la suma de los votos por partido sea igual a la de la candidatura PAN PRI PRD