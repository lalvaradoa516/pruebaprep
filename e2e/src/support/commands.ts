declare namespace Cypress {
    interface Chainable<Subject> {
      addValueBetweenSteps(key, value);
      returnValuesBetweenSteps(keys);
      returnAllValuesBetweenSteps();
    }
  }
  const data = {};
  Cypress.Commands.add('addValueBetweenSteps', (key, value) => {
    data[key] = value;
  });
  Cypress.Commands.add('returnValuesBetweenSteps', (keys) => {
    const dataReturn =  {};
    keys.forEach(key => {
      dataReturn[key]=data[key];
    });
    return dataReturn;
  });
  Cypress.Commands.add('returnAllValuesBetweenSteps', () => {
    return data;
  });
  