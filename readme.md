Instrucciones para ejecutar

0. posicionarse con la terminal en la raiz del proyecto
1. npm i (instalar dependencias)
2. npx cypress open -P ./e2e --config-file=cypress-dev.config.js (ejecutar cypress)
3. Elegir e2e test
4. Elegir navegador
5. Elegir la prueba a ejecutar

El codigo de las pruebas se encuentra en el folder e2e/src/integration
1. Archivo feature para las instrucciones en gherkin
2. Archivos ts para el codigo fuente

Screenshot
![Alt text](e2e/src/fixtures/capturaPrep.png?raw=true "Optional Title")